#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "mysh.h"


// Split cmd into array of parameters 
void parseCommand(char* command, char** parameters){
	int i ;
	for(i = 0; i < 10; i++){
		parameters[i] = strsep(&command,  " ");
		if(parameters[i] == NULL){
			break;
		}
	}

} 

int executeCmd(char** parameters){
	int i = 0;
	int j = 0;
	int k = 0;
	int outRedirection = 0;
	int inRedirection = 0;


	int pipe_num = 0;
	char ** commands[255];
	

	//Fork process
	pid_t pid = fork();

	commands[0] = parameters;

	//Error
	if(pid == -1){
		char* error = strerror(errno);
		printf("fork: %s\n", error);
		return 1;
	}

	//Child process -------------------------------------------------------------------------------------------
	else if (pid == 0){


		while (parameters[i] != NULL){


			if(parameters[i][0] == '|'){
				pipe_num++;
				parameters[i] = NULL;

				commands[pipe_num] = &(parameters[i+1]);
			}
			i++;
		}

		int des_p[255][2];
		i=0;

		while (commands[i]) {

			if(pipe(des_p[i]) == -1) {
				perror("Pipe failed");
				exit(1);
			}
			if(fork() == 0){
				outRedirection = outputRedirection(commands[i]);
				inRedirection = inputRedirection(commands[i]);
	
				if((i!=0) && (inRedirection == 0)) { //if exists previous 
					// take the input from pipe and not from the SDTIN
					dup2(des_p[i-1][0],0);
					// close all ends of pipes
				}
				if((commands[i+1]!=NULL) && (outRedirection == 0)){ // if exists next
					dup2(des_p[i][1],1);
				}
				// close all ends of pipes
				for (j =0; j<=i; j++ ){
					close(des_p[j][0]);       //closing pipe read
					close(des_p[j][1]);
				}
				execvp(commands[i][0], commands[i]);
				perror("execvp of command failed");
				exit(1);		  
			}
			i++;
		}
		// close all ends of pipes
		for (j =0; j<=i; j++ ){
			close(des_p[j][0]);       //closing pipe read
			close(des_p[j][1]);
		}	
		for (k =0; k <= pipe_num; k++){
			wait(0);
		}
		return 0;
	}

	//---------------------------------------------------------------------------------------------------------------
	//Parent process
	else{
		//Wait for child process to finish
		int childStatus;
		waitpid(pid, &childStatus, 0);
		return 1;
	}
}


int outputRedirection(char** parameters){
	int out;
	int i = 0;
	while (parameters[i] != NULL){
		if(parameters[i][0] == '>'){
			
			out = open(parameters[i+1], O_CREAT| O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
			if (out < 0){
				fprintf(stderr, "Error opening output file\n");
				exit(1);
			}
			parameters[i] = NULL;
			parameters[i+1]= NULL;


			// replace standard output with output file
			dup2(out, 1);
			close(out);
			return 1;
		}
		i++;
	}	
	return 0;
}

int inputRedirection(char** parameters){
	int in;
	int i = 0;
	while (parameters[i] != NULL){
		if(parameters[i][0] == '<'){ 
			// open input and output files
			in = open(parameters[i+1], O_RDONLY);
			if (in < 0){
				fprintf(stderr, "Error opening input file\n");
				exit(1);
			}

			int j = i;
			while (parameters[j] != NULL){
				parameters[j] = parameters[j+1];
				j++;
			}
			 // replace standard input with input file
			dup2(in, 0);
			close(in);
			return 1;
		}
		i++;
	}	
	return 0;
}
