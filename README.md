#Description

This a semester project for the Operating Systems laboratory at the Informatics Deparment of the Athens University of Economics and Business. The goal was to implement of a simple Unix shell, which
 a) displays a prompt, 
 b) reads a command and 
 c) creates appropriate processes for execution of the given command, including output redirection and pipes.


For example, the shell will execute the commands of the form:
`ls`

`ls -l`

`sort < in.txt > out.txt`

`ls -l /home/user/Downloads | sort > listing.txt`

`ls -l /home/user/Downloads | sort | wc > count.txt`


Installation
-

In order to clone the repository open a command line and issue

`git clone https://bitbucket.org/EleniMangiorou/unix_shell.git`

Enter the repository directory

`cd unix_shell`

To build the program run

`gcc -o mysh mysh.c mysh_common.c`

And run the program with the command

`./mysh`


