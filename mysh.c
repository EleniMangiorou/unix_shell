#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "mysh.h"

 
// Split cmd into array of parameters 

int main(){
	char cmd [255];
	char* params[10];
	
	while(1){
		//Print command prompt
		printf("%s", "myshell> ");
		
		//Read command from standard input, exit on Ctrl + D
		if(fgets(cmd, sizeof(cmd), stdin) == NULL)
			break;
		
		//Remove trailing newline character, if any
		if(cmd[strlen(cmd)-1] == '\n'){
			cmd[strlen(cmd)-1] = '\0';
		}
		
		// Split cmd into array of parameters
		parseCommand(cmd, params);
		
		//Exit
		if(strcmp(params[0], "exit") == 0)
			break;
			
		//Execute command
		if (executeCmd(params) == 0)
			break;
	}
	
	return 0;
}