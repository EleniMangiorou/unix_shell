#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


// Split cmd into array of parameters 
void parseCommand(char* command, char** parameters);

int executeCmd(char** parameters);

int outputRedirection(char** parameters);

int inputRedirection(char** parameters);

